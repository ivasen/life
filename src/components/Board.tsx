import React from 'react';
import Grid from './Grid';

const DEFAULT_GRID_SIZE = 40;
const DEFAULT_LIVE_PROBABILITY = 0.5;

interface BoardState {
  gridSize: number;
  liveProbability: number;
}

class Board extends React.Component<{}, BoardState> {
  childCallables?: { [index: string]: () => void };

  constructor(props: any) {
    super(props);
    this.state = {
      gridSize: DEFAULT_GRID_SIZE,
      liveProbability: DEFAULT_LIVE_PROBABILITY,
    };
  }

  render() {
    const { gridSize, liveProbability } = this.state;

    const buttonClasses = [
      'bg-blue-500 hover:border-transparent hover:bg-blue-600 font-bold',
      'py-2 px-4 rounded-lg col-span-2 text-white',
    ].join(' ');

    return (
      <div id='board' className='flex flex-col space-y-2.5 p-2'>
        <div className='grid grid-cols-4 gap-3'>
          <button
            className={buttonClasses}
            onClick={() => this.childCallables?.random()}
          >
            Randomize
          </button>
          <button
            className={buttonClasses}
            onClick={() => this.childCallables?.clear()}
          >
            Clear
          </button>
          <button
            className={buttonClasses}
            onClick={() => this.childCallables?.pause()}
          >
            Pause
          </button>
          <button
            className={buttonClasses}
            onClick={() => this.childCallables?.start()}
          >
            Start
          </button>
        </div>
        <div className=''>
          <Grid
            setCallables={(callables) => (this.childCallables = callables)}
            liveProbability={liveProbability}
            gridSize={gridSize}
          />
        </div>
      </div>
    );
  }
}

export default Board;
