import Board from './components/Board';

function App() {
  return (
    <div
      id='app-root'
      className={[
        'min-h-screen',
        'dark:bg-gray-800 dark:text-white',
        'bg-gray-100 text-black',
      ].join(' ')}
    >
      <header className='p-10 shadow-xl'>
        <div className='container mx-auto'>
          <h1 className='font-bold font-sans text-2xl'>
            Conway's Game of Life
          </h1>
        </div>
      </header>
      <main className='container max-w-2xl mx-auto pt-5 space-y-4'>
        <div className='text-lg'>
          <p>
            To learn more about game's of life, see{' '}
            <a
              className='dark:text-blue-400 text-blue-600'
              href='https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life'
            >
              this Wikipedia link
            </a>
          </p>
        </div>
        <Board />
      </main>
    </div>
  );
}

export default App;
